
## mai nyan
This both is referred to as "mai_nyan", "MaiNyan", "Mai nyan", etc.

So, e.g., `util::mentions::mai_nyan()` checks if the message mentions this bot.



## Dependencies

### dpp
dpp is not built by this project; the system install is used.


Build from source:
- See: https://dpp.dev/buildlinux.html
- Version: https://github.com/brainboxdotcc/DPP/releases/tag/v10.0.26
- `> cd third_party/DPP-x`
- `> cmake -B ./build`
- `> cmake --build ./build -j`
- `> sudo cmake --install ./build`


CMake:
- https://dpp.dev/buildcmake.html
