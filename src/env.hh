#pragma once

#include <string>

#include <dpp/dpp.h>

namespace env {
	const std::string bot_token = "discord_bot_token_here";
	// i_message_content required to read message content
	// const uint64_t bot_intents = dpp::i_default_intents | dpp::i_message_content;
	const uint64_t bot_intents = dpp::i_all_intents;
	const uint64_t bot_user_id = 1160743219131318293;
}
