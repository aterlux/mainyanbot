#pragma once

#include <dpp/dpp.h>
#include <vector>
#include "env.hh"

namespace util::mentions {

inline auto mai_nyan(std::vector<std::pair<dpp::user, dpp::guild_member>> mentions) -> bool {
	for (const auto &mention : mentions) {
		if (mention.first.is_bot() && mention.second.user_id == env::bot_user_id) {
			return true;
		}
	}
	return false;
}
}
