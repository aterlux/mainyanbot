#pragma once

#include <algorithm>
#include <vector>

namespace util {

// Syntax sugar for finding element in vector
template <typename T> 
inline auto vec_find(std::vector<T>& vec, const T& element) -> bool 
{
	if (std::find(vec.begin(), vec.end(), element) != vec.end()) {
		return true;
	}

	return false;
}

}
