#include <string>
#include <iostream>

#include <dpp/dpp.h>

#include "env.hh"
#include "mentions.hh"
#include "util.hh"

	
int main() {
	dpp::cluster bot(env::bot_token, env::bot_intents);

	bot.on_log(dpp::utility::cout_logger());
	
	bot.on_slashcommand([](const dpp::slashcommand_t& event) {
		std::cout << "received commang\n";
		if (event.command.get_command_name() == "nyan_ping") {
			std::cout << "received ping\n";
			event.reply("Nyan Pong!");
			return;
		}
		if (event.command.get_command_name() == "nyan_ping2") {
			std::cout << "received ping\n";
			event.reply("Nyan Pong 2!");
			return;
		}
		if (event.command.get_command_name() == "ping") {
			std::cout << "received ping\n";
			event.reply("Ping Pong!");
			return;
		}
	});
	
	bot.on_ready([&bot](const dpp::ready_t& event) {
		std::cout << "bot ready\n";
		if (dpp::run_once<struct register_bot_commands>()) {

			dpp::slashcommand cmd_ping("ping", "Ping pong!", bot.me.id);
			dpp::slashcommand cmd_nyan_ping("nyan_ping", "Ping pong!", bot.me.id);
			dpp::slashcommand cmd_nyan_ping3("nyan_ping3", "Ping pong!", bot.me.id);
	 
			bot.global_bulk_command_create({ cmd_ping, cmd_nyan_ping, cmd_nyan_ping3 });

			std::cout << "registered commands\n";
		}
	});
	
	bot.start(dpp::st_wait);
}