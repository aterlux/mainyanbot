# Style Guides
Check the `.clang-format` and `.clang-tidy` files for current formatting and C++ style.

Currently it is based on WebKit with:
- tabs
- changes to param/argument alignment
- Stroustrup newlines on braces (differs to WebKit in adding newline before else)



## Git
Use present tense.

Copy the pre-commit hook:
- `> cp ./pre-commit ./.git/hooks/pre-commit`
